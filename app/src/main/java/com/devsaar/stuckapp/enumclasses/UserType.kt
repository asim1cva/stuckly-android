package com.devsaar.stuckapp.enumclasses

enum class UserType(val value:String) {
    Business("business"),
    Customer("customer")
}
