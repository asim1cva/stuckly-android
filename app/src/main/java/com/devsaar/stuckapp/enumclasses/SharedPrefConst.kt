package com.devsaar.stuckapp.enumclasses

enum class SharedPrefConst(val value:String) {
    UserDataKey("stucklyAppUser")
}
