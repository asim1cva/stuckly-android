package com.devsaar.stuckapp.enumclasses

enum class ResponseStatus(val value:Int) {
    Success(200),
    Error(400)
}
