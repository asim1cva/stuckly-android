package com.devsaar.stuckapp.sharedviewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ForgotPasswordSharedViewModel: ViewModel() {

    val email = MutableLiveData<String>()
    val code = MutableLiveData<Int>()

    fun setData(mail: String,verifyCode:Int) {
        email.value = mail
        code.value=verifyCode
    }
}