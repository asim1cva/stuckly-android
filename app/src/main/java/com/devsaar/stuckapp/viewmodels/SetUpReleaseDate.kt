package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.auth.AuthResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class SetUpReleaseDate : ViewModel() {
    private var setReleaseDateLiveData = MutableLiveData<AuthResponse?>()
    val setReleaseDateData: LiveData<AuthResponse?> get() = setReleaseDateLiveData

    init {
        setReleaseDateLiveData.value = null
    }

    fun getData(
        repository: Repository,  businessId: Int,
        date: String,
        message: String,
        detail: String,
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.setReleaseDate(businessId,date,message,detail)
            if (response.body() != null && response.isSuccessful) {
                setReleaseDateLiveData.postValue(response.body())
            }
        }
    }
}
