package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.auth.AuthResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class UserRegistration : ViewModel() {
    private var signUpLiveData = MutableLiveData<AuthResponse?>()
    val signUpData: LiveData<AuthResponse?> get() = signUpLiveData
    init {
        signUpLiveData.value=null
        }

    fun  getData(repository: Repository,role: String, userName: String, email: String
                 , password: String, phoneNumber: String, fcmToken: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.signUp(
                role,  userName, email, password, phoneNumber, fcmToken
            )
            if (response.body() != null && response.isSuccessful) {
                signUpLiveData.postValue(response.body())
            }
        }
    }
}
