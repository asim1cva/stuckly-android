package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.auth.AuthResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class Login : ViewModel() {
    private var loginLiveData = MutableLiveData<AuthResponse?>()
    val loginData: LiveData<AuthResponse?> get() = loginLiveData
    init {
        loginLiveData.value=null
        }

    fun  getData(repository: Repository, email:String, password:String, role:String, fcm_token: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.login(email,password,role,fcm_token)
            if (response.body() != null && response.isSuccessful) {
                loginLiveData.postValue(response.body())
            }
        }
    }
}
