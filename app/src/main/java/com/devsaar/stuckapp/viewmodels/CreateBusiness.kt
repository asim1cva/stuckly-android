package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.CreateBusinessJsonRequest
import com.devsaar.stuckapp.models.auth.AuthResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class CreateBusiness : ViewModel() {
    private var createBusinessLiveData = MutableLiveData<AuthResponse?>()
    val createBusinessData: LiveData<AuthResponse?> get() = createBusinessLiveData
    init {
        createBusinessLiveData.value=null
        }

    fun  getData(repository: Repository,  businessData: CreateBusinessJsonRequest) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.createBusiness(businessData)
            if (response.body() != null && response.isSuccessful) {
                createBusinessLiveData.postValue(response.body())
            }
        }
    }
}
