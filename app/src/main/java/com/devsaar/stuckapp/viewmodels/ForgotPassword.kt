package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.getbusinesslist.ForgotPasswordResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ForgotPassword : ViewModel() {
    private var forgotPasswordLiveData = MutableLiveData<ForgotPasswordResponse?>()
    val forgotPasswordData: LiveData<ForgotPasswordResponse?> get() = forgotPasswordLiveData
    init {
        forgotPasswordLiveData.value=null
        }

    fun  getData(repository: Repository, email:String) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.forgotPassword(email)
            if (response.body() != null && response.isSuccessful) {
                forgotPasswordLiveData.postValue(response.body())
            }
        }
    }
}
