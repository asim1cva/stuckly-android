package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.CreateBusinessJsonRequest
import com.devsaar.stuckapp.models.auth.AuthResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class UpdateBusiness : ViewModel() {
    private var updateBusinessLiveData = MutableLiveData<AuthResponse?>()
    val updateBusinessData: LiveData<AuthResponse?> get() = updateBusinessLiveData
    init {
        updateBusinessLiveData.value=null
        }

    fun  getData(repository: Repository,  businessData: CreateBusinessJsonRequest) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.updateBusiness(businessData)
            if (response.body() != null && response.isSuccessful) {
                updateBusinessLiveData.postValue(response.body())
            }
        }
    }
}
