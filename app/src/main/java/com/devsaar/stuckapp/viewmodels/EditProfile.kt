package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.auth.AuthResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class EditProfile : ViewModel() {
    private var editProfileLiveData = MutableLiveData<AuthResponse?>()
    val editProfileData: LiveData<AuthResponse?> get() = editProfileLiveData

    init {
        editProfileLiveData.value = null
    }

    fun getData(
        repository: Repository, userId: Int, userName: String, email: String, dateOfBirth: String,
        phoneNumber: String
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.editProfile(
                userId, userName, email, dateOfBirth, phoneNumber
            )
            if (response.body() != null && response.isSuccessful) {
                editProfileLiveData.postValue(response.body())
            }
        }
    }
}
