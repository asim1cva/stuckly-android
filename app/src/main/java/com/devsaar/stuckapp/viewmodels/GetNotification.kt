package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class GetNotification : ViewModel() {
    private var getNotificationLiveData = MutableLiveData<GetBusinessListResponse?>()
    val getNotificationData: LiveData<GetBusinessListResponse?> get() = getNotificationLiveData

    init {
        getNotificationLiveData.value = null
    }

    fun getData(repository: Repository, userId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.getNotification(userId)
            if (response.body() != null && response.isSuccessful) {
                getNotificationLiveData.postValue(response.body())
            }
        }
    }
}
