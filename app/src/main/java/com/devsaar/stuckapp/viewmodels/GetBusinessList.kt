package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class GetBusinessList : ViewModel() {
    private var getBusinessListLiveData = MutableLiveData<GetBusinessListResponse?>()
    val getBusinessListData: LiveData<GetBusinessListResponse?> get() = getBusinessListLiveData
    init {
        getBusinessListLiveData.value=null
        }

    fun  getData(repository: Repository, userId:Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.getBusinessList(userId)
            if (response.body() != null && response.isSuccessful) {
                getBusinessListLiveData.postValue(response.body())
            }
        }
    }
}
