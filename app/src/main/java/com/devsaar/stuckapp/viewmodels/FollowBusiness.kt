package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.auth.AuthResponse
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class FollowBusiness : ViewModel() {
    private var followBusinessLiveData = MutableLiveData<AuthResponse?>()
    val followBusinessData: LiveData<AuthResponse?> get() = followBusinessLiveData

    init {
        followBusinessLiveData.value = null
    }

    fun getData(repository: Repository, userId: Int, businessId: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.followBusiness(businessId, userId)
            if (response.body() != null && response.isSuccessful) {
                followBusinessLiveData.postValue(response.body())
            }
        }
    }
}
