package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.auth.AuthResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class ChangePassword : ViewModel() {
    private var changePasswordLiveData = MutableLiveData<AuthResponse?>()
    val changePasswordData: LiveData<AuthResponse?> get() = changePasswordLiveData
    init {
        changePasswordLiveData.value=null
        }

    fun  getData(repository: Repository, email:String, newPassword:String) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.changePassword(email,newPassword)
            if (response.body() != null && response.isSuccessful) {
                changePasswordLiveData.postValue(response.body())
            }
        }
    }
}
