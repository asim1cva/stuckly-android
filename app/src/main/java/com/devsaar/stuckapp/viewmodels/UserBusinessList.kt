package com.devsaar.stuckapp.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListResponse
import com.devsaar.stuckapp.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class UserBusinessList : ViewModel() {
    private var userBusinessListLiveData = MutableLiveData<GetBusinessListResponse?>()
    val userBusinessListData: LiveData<GetBusinessListResponse?> get() = userBusinessListLiveData
    init {
        userBusinessListLiveData.value=null
        }

    fun  getData(repository: Repository, userId:Int) {
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.userBusinessList(userId)
            if (response.body() != null && response.isSuccessful) {
                userBusinessListLiveData.postValue(response.body())
            }
        }
    }
}
