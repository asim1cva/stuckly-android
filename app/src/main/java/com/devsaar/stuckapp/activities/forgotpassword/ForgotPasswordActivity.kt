package com.devsaar.stuckapp.activities.forgotpassword

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.base.BaseActivityWithoutVM
import com.devsaar.stuckapp.databinding.ActivityForgotPasswordBinding

class ForgotPasswordActivity : BaseActivityWithoutVM<ActivityForgotPasswordBinding>() {
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        init()
    }

    override fun getViewBinding(): ActivityForgotPasswordBinding =
        ActivityForgotPasswordBinding.inflate(layoutInflater)

    override fun init() {

        val navFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navFragment.navController

    }

    override fun setListener() {

    }


}