package com.devsaar.stuckapp.activities.customer

import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.base.BaseActivityWithoutVM
import com.devsaar.stuckapp.databinding.ActivityCustomerDashboardBinding

class CustomerDashboardActivity : BaseActivityWithoutVM<ActivityCustomerDashboardBinding>() {
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        init()
    }

    override fun getViewBinding(): ActivityCustomerDashboardBinding =
        ActivityCustomerDashboardBinding.inflate(layoutInflater)

    override fun init() {

        val navFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navFragment.navController
        mViewBinding.navView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.notificationFragment -> {
                    mViewBinding.navView.visibility = View.GONE
                }
                R.id.customerSideDetailFragment -> {
                    mViewBinding.navView.visibility = View.GONE
                }
                else -> {
                    mViewBinding.navView.visibility = View.VISIBLE
                }

            }

        }

    }

    override fun setListener() {

    }


}