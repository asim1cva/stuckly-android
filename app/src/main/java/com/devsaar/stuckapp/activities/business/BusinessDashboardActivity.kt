package com.devsaar.stuckapp.activities.business

import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.base.BaseActivityWithoutVM
import com.devsaar.stuckapp.databinding.ActivityBusinessDashboardBinding

class BusinessDashboardActivity : BaseActivityWithoutVM<ActivityBusinessDashboardBinding>() {
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        init()
        setListener()


    }

    override fun getViewBinding(): ActivityBusinessDashboardBinding =
        ActivityBusinessDashboardBinding.inflate(layoutInflater)

    override fun init() {
        val navFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_driver_job) as NavHostFragment
        navController = navFragment.navController
        mViewBinding.navView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.detailFragment -> {
                    mViewBinding.navView.visibility = View.GONE
                }
                R.id.updateBusinessFragment -> {
                    mViewBinding.navView.visibility = View.GONE
                }
                R.id.setupReleaseDateFragment -> {
                    mViewBinding.navView.visibility = View.GONE
                }
                else -> {
                    mViewBinding.navView.visibility = View.VISIBLE
                }

            }
        }
    }

    override fun setListener() {

    }
}