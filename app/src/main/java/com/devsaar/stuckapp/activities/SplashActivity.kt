package com.devsaar.stuckapp.activities

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.devsaar.stuckapp.activities.auth.AuthActivity
import com.devsaar.stuckapp.base.BaseActivityWithoutVM
import com.devsaar.stuckapp.databinding.ActivitySplashBinding

class SplashActivity : BaseActivityWithoutVM<ActivitySplashBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        init()
    }

    override fun getViewBinding(): ActivitySplashBinding =
        ActivitySplashBinding.inflate(layoutInflater)

    override fun init() {

        Handler(Looper.getMainLooper()).postDelayed({
            navigateAndClearBackStack(AuthActivity::class.java)

        }, 3000)


    }

    override fun setListener() {

    }

}
