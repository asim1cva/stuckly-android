package com.devsaar.stuckapp.activities

import android.os.Bundle
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.adapter.dateslider.DateAdapter
import com.devsaar.stuckapp.base.BaseActivityWithoutVM
import com.devsaar.stuckapp.databinding.ActivitySampleDateBinding
import com.devsaar.stuckapp.models.dateslider.Date
import java.text.SimpleDateFormat
import java.util.*

class SampleDateActivity : BaseActivityWithoutVM<ActivitySampleDateBinding>() {
    private var dataList = mutableListOf<Date>()
    private val adapter by lazy {
        DateAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        init()
        setListener()
    }

    override fun getViewBinding(): ActivitySampleDateBinding =
        ActivitySampleDateBinding.inflate(layoutInflater)

    override fun init() {
        mViewBinding.rv.adapter = adapter
        setData()
        adapter.addItems(dataList)
    }

    //for getting data and setting data in data list
    private fun setData() {
        val c: Calendar = Calendar.getInstance()
        // format for day date of week
        val day = SimpleDateFormat("dd")
        // format for day name of week
        val dayOfTheWeek = SimpleDateFormat("EEEE")

        val fullDate = SimpleDateFormat("y-M-d ")
        com.devsaar.stuckapp.utils.Utils

        c.add(Calendar.DATE, 0)
        val day1Name = dayOfTheWeek.format(c.time)
        val day1Date = day.format(c.time)
        addData(day1Name, day1Date,fullDate.format(c.time) ,true)
//        binding.tvDate.text = "$day1Name $day1Date "

        c.add(Calendar.DATE, 1)
        val day2Name = dayOfTheWeek.format(c.time)
        val day2Date = day.format(c.time)
        addData(day2Name, day2Date, fullDate.format(c.time) ,false)

        c.add(Calendar.DATE, 1)
        val day3Name = dayOfTheWeek.format(c.time)
        val day3Date = day.format(c.time)
        addData(day3Name, day3Date, fullDate.format(c.time) ,false)

        c.add(Calendar.DATE, 1)
        val day4Name = dayOfTheWeek.format(c.time)
        val day4Date = day.format(c.time)
        addData(day4Name, day4Date, fullDate.format(c.time) ,false)

        c.add(Calendar.DATE, 1)
        val day5Name = dayOfTheWeek.format(c.time)
        val day5Date = day.format(c.time)
        addData(day5Name, day5Date,fullDate.format(c.time) , false)

        c.add(Calendar.DATE, 1)
        val day6Name = dayOfTheWeek.format(c.time)
        val day6Date = day.format(c.time)
        addData(day6Name, day6Date, fullDate.format(c.time) ,false)

        c.add(Calendar.DATE, 1)
        val day7Name = dayOfTheWeek.format(c.time)
        val day7Date = day.format(c.time)
        addData(day7Name, day7Date,fullDate.format(c.time) , false)
    }

    //for setting data in data list
    private fun addData(day: String, dayDate: String,date: String, selected: Boolean) {
        val dayName = when (day) {
            "Monday" -> {
                "Mon"
            }
            "Tuesday" -> {
                "Tue"
            }
            "Wednesday" -> {
                "Wed"
            }
            "Thursday" -> {
                "Thu"
            }
            "Friday" -> {
                "Fri"
            }
            "Saturday" -> {
                "Sat"
            }
            "Sunday" -> {
                "Sun"
            }
            else -> ""
        }
//        dataList.add(Date(dayName, date, selected))
    }

    override fun setListener() {
        // called on date clicked and on click listener from adapter
        adapter.listener = { view, item, position ->
            when (view.id) {
                R.id.iv_cv -> {
                    mViewBinding.tvDate.text="${dataList[position].day} ${dataList[position].date}"
                    for (i in 0..6) {
                        when (i) {
                            position -> {
                                dataList[i].selected = true
                            }
                            else -> {
                                dataList[i].selected = false
                            }
                        }
                        adapter.addItems(dataList)
                    }
                }
            }
        }
    }
}