package com.devsaar.stuckapp.activities.auth

import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.activities.business.BusinessDashboardActivity
import com.devsaar.stuckapp.activities.customer.CustomerDashboardActivity
import com.devsaar.stuckapp.base.BaseActivityWithoutVM
import com.devsaar.stuckapp.databinding.ActivityAuthBinding
import com.devsaar.stuckapp.enumclasses.SharedPrefConst
import com.devsaar.stuckapp.enumclasses.UserType
import com.devsaar.stuckapp.sharedPref.SharedPref

class AuthActivity : BaseActivityWithoutVM<ActivityAuthBinding>() {
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        init()
    }

    override fun getViewBinding(): ActivityAuthBinding =
        ActivityAuthBinding.inflate(layoutInflater)

    override fun init() {
        if (SharedPref.instance?.getLoginData(SharedPrefConst.UserDataKey.value) != null) {
            when (SharedPref.instance?.getLoginData(SharedPrefConst.UserDataKey.value)!!.role) {
                UserType.Business.value -> {
                    navigateAndClearBackStack(
                        BusinessDashboardActivity::class.java
                    )
                }
                UserType.Customer.value -> {
                    navigateAndClearBackStack(
                        CustomerDashboardActivity::class.java
                    )
                }
            }

        }

        val navFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navFragment.navController

    }

    override fun setListener() {

    }


}