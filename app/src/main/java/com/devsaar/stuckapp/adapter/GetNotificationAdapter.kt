package com.devsaar.stuckapp.adapter

import android.content.Context
import androidx.navigation.NavController
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.base.BaseRecyclerViewAdapter
import com.devsaar.stuckapp.databinding.IvNotificationBinding
import com.devsaar.stuckapp.fragments.customer.NotificationFragmentDirections
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListData
import com.devsaar.stuckapp.utils.Utils

class GetNotificationAdapter(val context: Context, private val navController: NavController) :
    BaseRecyclerViewAdapter<GetBusinessListData, IvNotificationBinding>() {
    override fun getLayout(): Int = R.layout.iv_notification

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<IvNotificationBinding>, position: Int
    ) {
        holder.binding.apply {
            notification = items[position]
            tvReleaseDate.setText("Release Date : ${items[position].release_date}")
            if (!items[position].images.isNullOrEmpty()) {
                Utils.setImageWithCoil(items[position].images[0].image_url, iv)
            }
            lLayout.setOnClickListener {
                val action =
                    NotificationFragmentDirections.actionNotificationFragmentToCustomerSideDetailFragment(
                        items[position], context.getString(R.string.notification)
                    )
                navController.navigate(action)
            }

        }
    }


}
