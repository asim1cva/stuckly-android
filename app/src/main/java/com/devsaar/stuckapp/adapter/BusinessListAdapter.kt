package com.devsaar.stuckapp.adapter

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.base.BaseRecyclerViewAdapter
import com.devsaar.stuckapp.databinding.IvBusinessBinding
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListData
import com.devsaar.stuckapp.utils.Utils

class BusinessListAdapter (val context: Context, private val navController: NavController) :
    BaseRecyclerViewAdapter<GetBusinessListData, IvBusinessBinding>() {
    override fun getLayout(): Int = R.layout.iv_business

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<IvBusinessBinding>, position: Int
    ) {
        holder.binding.apply {


            business = items[position]
            tvFollowersCount.setText(items[position].followers_count.toString()+" ")
            if (!items[position].images.isNullOrEmpty()) {
                Utils.setImageWithCoil(items[position].images[0].image_url, iv)
            }
            lLayout.setOnClickListener {
                listener?.invoke(it, items[position], position)
            }

        }
    }


}
