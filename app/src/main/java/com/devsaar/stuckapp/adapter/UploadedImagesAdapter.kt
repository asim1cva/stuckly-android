package com.devsaar.stuckapp.adapter

import android.graphics.Bitmap
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.base.BaseRecyclerViewAdapter
import com.devsaar.stuckapp.databinding.IvUploadImagesBinding
import com.devsaar.stuckapp.utils.Utils

class UploadedImagesAdapter :
    BaseRecyclerViewAdapter<Bitmap, IvUploadImagesBinding>() {
    val base64List = mutableListOf<String?>()
    override fun getLayout(): Int = R.layout.iv_upload_images

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<IvUploadImagesBinding>, position: Int
    ) {
        holder.binding.apply {
            uploadImage = items[position]
            val imageBitmap = uploadImage
            base64List.add(Utils.encodeImage(imageBitmap!!))
            ivUpload.setImageBitmap(items[position])
            btnClose.setOnClickListener {
                //TODO:Remove two adjacent item when first of them item remove index out bound error
                removeItems(position)
            }
        }
    }


}