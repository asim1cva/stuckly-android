package com.devsaar.stuckapp.adapter

import com.devsaar.stuckapp.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.NavController
import com.devsaar.stuckapp.fragments.business.BusinessListFragmentDirections
import com.devsaar.stuckapp.fragments.business.DetailFragmentDirections
import com.devsaar.stuckapp.fragments.customer.CustomerSideDetailFragmentDirections
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListImage
import com.devsaar.stuckapp.utils.Utils
import com.github.siyamed.shapeimageview.RoundedImageView
import com.smarteist.autoimageslider.SliderViewAdapter


class SliderAdapter(var images: List<GetBusinessListImage>,var navController: NavController) :
    SliderViewAdapter<SliderAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup): Holder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.iv_slider, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(viewHolder: Holder, position: Int) {
//        viewHolder.imageView.setImageResource(images[position])
        viewHolder.apply {
            Utils.setImageWithCoil(images[position].image_url, imageView)
            imageView.setOnClickListener {
                try {
                    val action=DetailFragmentDirections.actionDetailFragmentToImageFullViewFragment(images[position].image_url)
                    navController.navigate(action)
                }
                catch (e:Exception) {
                    val action=CustomerSideDetailFragmentDirections.actionCustomerSideDetailFragmentToImageFullViewFragment(images[position].image_url)
                    navController.navigate(action)
                }
            }
        }

    }

    override fun getCount(): Int {
        return images.size
    }

    inner class Holder(itemView: View) : ViewHolder(itemView) {
        var imageView: RoundedImageView

        init {
            imageView = itemView.findViewById(R.id.iv_image)
        }
    }
}