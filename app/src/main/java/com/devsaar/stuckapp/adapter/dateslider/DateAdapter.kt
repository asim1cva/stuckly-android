package com.devsaar.stuckapp.adapter.dateslider

import android.content.Context
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.base.BaseRecyclerViewAdapter
import com.devsaar.stuckapp.databinding.IvDateBinding
import com.devsaar.stuckapp.models.dateslider.Date
import com.devsaar.stuckapp.utils.Utils

class DateAdapter(val context: Context) : BaseRecyclerViewAdapter<Date, IvDateBinding>() {
    override fun getLayout(): Int = R.layout.iv_date

    override fun onBindViewHolder(
        holder: Companion.BaseViewHolder<IvDateBinding>, position: Int
    ) {
        holder.binding.apply {
            date = items[position]
            //apply logic of selection
            when (items[position].selected) {
                false -> {
                    Utils.setVisibility(cvUnselected, cvSelected)
                }
                true -> {
                    Utils.setVisibility(cvSelected, cvUnselected)
                }
            }

            ivCv.setOnClickListener {
                listener?.invoke(it, items[position], position)
            }


        }


    }

}