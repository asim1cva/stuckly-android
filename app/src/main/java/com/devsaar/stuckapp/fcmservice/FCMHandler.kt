package com.devsaar.stuckapp.fcmservice

import com.google.firebase.installations.FirebaseInstallations
import com.google.firebase.messaging.FirebaseMessaging
import java.io.IOException

class FCMHandler {
    fun enableFCM() {
        // Enable FCM via enable Auto-init service which generate new token and receive in FCMService
        FirebaseMessaging.getInstance().setAutoInitEnabled(true)
    }
    fun disableFCM() {
        // Disable auto init
        FirebaseMessaging.getInstance().setAutoInitEnabled(false)
        Thread {
            try {
                FirebaseMessaging.getInstance().deleteToken()
                // Remove InstanceID initiate to unsubscribe all topic
                FirebaseInstallations .getInstance().delete()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }.start()
    }
}