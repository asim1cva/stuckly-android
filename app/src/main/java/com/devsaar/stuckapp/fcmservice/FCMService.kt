package com.devsaar.stuckapp.fcmservice

import android.util.Log
import com.devsaar.stuckapp.sharedPref.SharedPrefHelper
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class FCMService : FirebaseMessagingService() {


    val TAG = "MyFirebase"
    val count = 0

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        //Retrieve the values
        //Retrieve the values


        val data = p0.data
        val body = data["body"]
        val title = data["title"]



        Log.d(TAG, "data: $data")
        Log.d(TAG, "title: $title")
        Log.d(TAG, "body: $body")


    }

    override fun onNewToken(token: String) {
        Log.d(TAG, "Refreshed token: $token")

        SharedPrefHelper.instance!!.saveDeviceToken(token)


        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        sendRegistrationToServer(token)
    }


    private fun sendRegistrationToServer(token: String) {
        // TODO: Implement this method to send token to your app server.
        Log.d(TAG, "sendRegistrationTokenToServer($token)")
    }

    companion object {
        private const val TAG = "firebaseMessageService"
    }


}
