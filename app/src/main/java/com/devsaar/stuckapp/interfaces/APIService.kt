package com.devsaar.stuckapp.interfaces


import com.devsaar.stuckapp.models.CreateBusinessJsonRequest
import com.devsaar.stuckapp.models.auth.AuthResponse
import com.devsaar.stuckapp.models.getbusinesslist.ForgotPasswordResponse
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListResponse
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*
import retrofit2.http.Body

import retrofit2.http.POST


interface APIService {


    @FormUrlEncoded
    @POST("userRegister")
    suspend fun signUp(
        @Field("role") role: String,
        @Field("user_name") userName: String,
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("phone_number") phoneNumber: String,
        @Field("fcm_token") fcmToken: String,
    ): Response<AuthResponse>

    @FormUrlEncoded
    @POST("userLogin")
    suspend fun userLogin(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("role") role: String,
        @Field("fcm_token") fcmToken: String
    ): Response<AuthResponse>


    @FormUrlEncoded
    @POST("editProfile")
    suspend fun editProfile(
        @Field("user_id") userId: Int,
        @Field("user_name") user_name: String,
        @Field("email") email: String,
        @Field("date_of_birth") dateOfBirth: String,
        @Field("phone_number") phoneNumber: String,
    ): Response<AuthResponse>

    @FormUrlEncoded
    @POST("userBusinessList")
    suspend fun userBusinessList(
        @Field("user_id") userId: Int
    ): Response<GetBusinessListResponse>

    @FormUrlEncoded
    @POST("getBusinessList")
    suspend fun getBusinessList(
        @Field("user_id") userId: Int
    ): Response<GetBusinessListResponse>



    @FormUrlEncoded
    @POST("setReleaseDate")
    suspend fun setReleaseDate(
        @Field("business_id") businessId: Int,
        @Field("date") date: String,
        @Field("message") message: String,
        @Field("detail") detail: String,
    ): Response<AuthResponse>

    @Headers("Content-Type: application/json")
    @POST("createBusiness")
    suspend fun createBusiness(
        @Body businessData: CreateBusinessJsonRequest
    ): Response<AuthResponse>

    @Headers("Content-Type: application/json")
    @POST("updateBusiness")
    suspend fun updateBusiness(
        @Body businessData: CreateBusinessJsonRequest
    ): Response<AuthResponse>

    @FormUrlEncoded
    @POST("followBusiness")
    suspend fun followBusiness(
        @Field("business_id") businessId: Int,
        @Field("user_id") userId: Int
    ): Response<AuthResponse>

    @FormUrlEncoded
    @POST("forgotPassword")
    suspend fun forgotPassword(
        @Field("email") email: String,
    ): Response<ForgotPasswordResponse>

    @FormUrlEncoded
    @POST("changePassword")
    suspend fun changePassword(
        @Field("email") email: String,
        @Field("new_password") newPassword: String,
    ): Response<AuthResponse>

    @FormUrlEncoded
    @POST("getNotification")
    suspend fun getNotification(
        @Field("user_id") userId: Int
    ): Response<GetBusinessListResponse>

}