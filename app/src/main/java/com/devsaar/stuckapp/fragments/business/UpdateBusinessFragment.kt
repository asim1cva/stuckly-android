package com.devsaar.stuckapp.fragments.business

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.adapter.UploadedImagesAdapter
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentCreateBusinessBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.models.CreateBusinessJsonRequest
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.utils.Utils
import com.devsaar.stuckapp.viewmodels.UpdateBusiness
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.*
import java.lang.Exception
import kotlin.coroutines.CoroutineContext


class UpdateBusinessFragment : BaseFragment<FragmentCreateBusinessBinding, UpdateBusiness>() {
    private lateinit var navController: NavController
    private val imageList = mutableListOf<Bitmap>()
    var base64List = mutableListOf<String?>()
    val args: UpdateBusinessFragmentArgs by navArgs()
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_IMAGE_UPLOAD = 2
    private val uploadedImagesAdapter by lazy {
        UploadedImagesAdapter()
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentCreateBusinessBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        binding.apply {
            tvUpload.text = getString(R.string.replace_images)
            btnBack.visibility = View.VISIBLE
            business = args.businessData
            btnCreate.text = getString(R.string.update)
            tvPageName.text = getString(R.string.UpdateBusiness)
            rvUploadedImages.adapter = uploadedImagesAdapter
            uploadedImagesAdapter.addItems(imageList)
            loadImage()


        }
    }


    private fun loadImage() {
        var t: Bitmap? = null
        CoroutineScope(Dispatchers.IO).async {
            for (i in args.businessData.images) {

                t = Utils.getBitmapFromUrl(i.image_url)

                withContext(Dispatchers.Main) {
                    imageList.add(t!!)
                    uploadedImagesAdapter.notifyDataSetChanged()
                }
            }
        }


    }


    override fun getViewModel(): Class<UpdateBusiness> = UpdateBusiness::class.java
    override fun setListener() {
        binding.apply {

            btnCreate.setOnClickListener {
                loadUpdateBusinessResponse()
            }
            ivUploadImage.setOnClickListener {
                showMediaSelectionDialog()
            }
            btnBack.setOnClickListener {
                navController.navigateUp()
            }

        }
    }

    private fun loadUpdateBusinessResponse() {
        setVisibility(binding.pb, binding.btnCreate)
        base64List.clear()
        for (i in uploadedImagesAdapter.items) {
            base64List.add(Utils.encodeImage(i))
        }
        if (isNetworkConnected()) {
            val businessData = CreateBusinessJsonRequest(
                getText(binding.etBusinessName),
                getText(binding.etDescription),
                getText(binding.etWebLink),
                base64List,
                null,
                args.businessData.id
            )


            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[UpdateBusiness::class.java]
            viewModel.getData(
                repository,
                businessData
            )


            viewModel.updateBusinessData.observe(viewLifecycleOwner, {
                setVisibility(binding.btnCreate, binding.pb)
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            showSnackBar(message)
                        }
                        ResponseStatus.Success.value -> {
                            showSnackBar(message)
                            val action =
                                UpdateBusinessFragmentDirections.actionUpdateBusinessFragmentToBusinessListFragment()
                            navController.navigate(action)
                        }
                    }
                }


            })

        } else {
            binding.pb.visibility = View.GONE
        }

    }

    private fun showMediaSelectionDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(resources.getString(R.string.supporting_text))
            .setNegativeButton(resources.getString(R.string.Gallery)) { dialog, which ->
                openGallery()
            }
            .setPositiveButton(resources.getString(R.string.camera)) { dialog, which ->
                takePicture()
            }
            .show()
    }

    private fun openGallery() {


        val gallery = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(gallery, REQUEST_IMAGE_UPLOAD)

    }


    private fun takePicture() {
        val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        try {
            this.startActivityForResult(pictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: ActivityNotFoundException) {
            showToast(e.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_UPLOAD && data != null) {
            val bitmap: Bitmap =
                MediaStore.Images.Media.getBitmap(context?.getContentResolver(), data.data)
            binding.ivUploadImage.setImageBitmap(bitmap)
            imageList.add(bitmap as Bitmap)
            uploadedImagesAdapter.notifyDataSetChanged()
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE) {

            if (data?.extras?.get("data") != null) {
                imageList.add(data.extras?.get("data") as Bitmap)
                uploadedImagesAdapter.notifyDataSetChanged()

            } else {
                showToast(getString(R.string.noimageselectd))
            }
        }
    }
}