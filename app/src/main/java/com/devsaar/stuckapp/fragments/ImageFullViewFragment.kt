package com.devsaar.stuckapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.base.BaseFragmentWithoutVM
import com.devsaar.stuckapp.databinding.FragmentImageFullViewBinding
import com.devsaar.stuckapp.databinding.FragmentSetupReleaseDateBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.utils.Utils
import com.devsaar.stuckapp.viewmodels.SetUpReleaseDate


class ImageFullViewFragment : BaseFragmentWithoutVM<FragmentImageFullViewBinding>() {
    val args: ImageFullViewFragmentArgs by navArgs()
    private lateinit var navController: NavController

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentImageFullViewBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        Utils.setSimpleImageWithCoil(args.imageUrl,binding.iv)
    }

    override fun setListener() {
        binding.apply {
            btnBack.setOnClickListener {
                navController.navigateUp()
            }

        }
    }

}