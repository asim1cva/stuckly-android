package com.devsaar.stuckapp.fragments.forgotpassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.base.BaseFragmentWithoutVM
import com.devsaar.stuckapp.databinding.FragmentForgotPasswordBinding
import com.devsaar.stuckapp.databinding.FragmentVerifyEmailBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.enumclasses.SharedPrefConst
import com.devsaar.stuckapp.fcmservice.FCMHandler
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedPref.SharedPref
import com.devsaar.stuckapp.sharedviewmodels.ForgotPasswordSharedViewModel
import com.devsaar.stuckapp.viewmodels.Login
import kotlinx.android.synthetic.main.fragment_login.*


class VerifyEmailFragment : BaseFragmentWithoutVM<FragmentVerifyEmailBinding>() {
    private lateinit var navController: NavController
    private lateinit var forgotPasswordSharedViewModel: ForgotPasswordSharedViewModel
    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentVerifyEmailBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        forgotPasswordSharedViewModel =
            ViewModelProvider(requireActivity()).get(ForgotPasswordSharedViewModel::class.java)
    }

    override fun setListener() {
        binding.apply {
            btnResetPassword.setOnClickListener {
                resetPassword()

            }

        }
    }


    fun resetPassword() {
        binding.apply {
            when {
                isEmpty(etCode) -> etCode.error = "Enter verification code"
                else -> {
                    var code = 0
                    forgotPasswordSharedViewModel.code.observe(viewLifecycleOwner, Observer {
                        // updating data in displayMsg
                        code = it
                    })
                    when (getText(etCode).toInt()) {
                        code -> {
                            val action =
                                VerifyEmailFragmentDirections.actionVerifyEmailFragmentToNewPasswordFragment()
                            navController.navigate(action)
                        }
                        else -> {
                            etCode.error = "Wrong verification code"
                        }
                    }

                }

            }
        }
    }


}