package com.devsaar.stuckapp.fragments.auth

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.devsaar.stuckapp.activities.business.BusinessDashboardActivity
import com.devsaar.stuckapp.activities.customer.CustomerDashboardActivity
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentSignUpBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.enumclasses.SharedPrefConst
import com.devsaar.stuckapp.enumclasses.UserType
import com.devsaar.stuckapp.fcmservice.FCMHandler
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedPref.SharedPref
import com.devsaar.stuckapp.sharedPref.SharedPrefHelper
import com.devsaar.stuckapp.viewmodels.UserRegistration
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging


class SignUpFragment : BaseFragment<FragmentSignUpBinding, UserRegistration>() {
    private lateinit var auth: FirebaseAuth
    private val args: SignUpFragmentArgs by navArgs()
    private lateinit var navController: NavController

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentSignUpBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getFCMToken()
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        auth = Firebase.auth
    }

    override fun getViewModel(): Class<UserRegistration> = UserRegistration::class.java
    override fun setListener() {
        binding.apply {
            tvSignIn.setOnClickListener {
                navController.navigateUp()
            }
            btnSignUp.setOnClickListener {
                signUp()
            }

        }
    }

    // Get FCM token
    private fun getFCMToken() {

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            //On token fetch fail
            if (!task.isSuccessful) {
                //msg_token_failed
                val token = task.exception.toString()
                SharedPrefHelper.instance!!.saveDeviceToken(token)
                //TODO:FCM not save

                return@OnCompleteListener
            }

            // Get new Instance ID token
            val newDeviceToken = task.result
            SharedPrefHelper.instance!!.saveDeviceToken(newDeviceToken)
            Log.d("SignUpFragment", " new Device token $newDeviceToken")
        })
    }

    private fun signUp() {
        val fcm = FCMHandler()
        fcm.enableFCM()
        binding.apply {
            when {
                isEmpty(etUserName) -> etUserName.error = "Enter Last Name"
                isEmpty(etMail) -> etMail.error = "Enter Email"
                isEmpty(etPhoneNumber) -> etPhoneNumber.setText("")
                isEmpty(etPassword) -> etPassword.error = "Enter User Password"
                else -> {
                    loadSignUpResponse()
                }

            }
        }
    }

    private fun loadSignUpResponse() {
        setVisibility(binding.pb, binding.btnSignUp)
        if (isNetworkConnected()) {


            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[UserRegistration::class.java]
            viewModel.getData(
                repository,
                args.userRole,
                getText(binding.etUserName),
                getText(binding.etMail),
                getText(binding.etPassword),
                getText(binding.etPhoneNumber),
                SharedPrefHelper.instance!!.deviceToken
            )
            Log.d("userRole", args.userRole)

            viewModel.signUpData.observe(viewLifecycleOwner, {
                setVisibility(binding.btnSignUp, binding.pb)
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            showSnackBar(message)
                        }
                        else -> {
                            SharedPref.instance?.saveLoginData(
                                SharedPrefConst.UserDataKey.value,
                                data
                            )
                            navigateToHomeFragment()
                        }
                    }
                }


            })

        } else {
            setVisibility(binding.btnSignUp, binding.pb)
        }

    }

    private fun navigateToHomeFragment() {

        when (args.userRole) {
            UserType.Business.value -> {
                navigateAndClearBackStack(
                    BusinessDashboardActivity::class.java
                )
            }
            UserType.Customer.value -> {
                navigateAndClearBackStack(
                    CustomerDashboardActivity::class.java
                )
            }
            else -> showSnackBar("not recognized")
        }
    }


}