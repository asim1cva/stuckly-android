package com.devsaar.stuckapp.fragments.forgotpassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.devsaar.stuckapp.activities.auth.AuthActivity
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentForgotPasswordBinding
import com.devsaar.stuckapp.databinding.FragmentNewPasswordBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.enumclasses.SharedPrefConst
import com.devsaar.stuckapp.fcmservice.FCMHandler
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedPref.SharedPref
import com.devsaar.stuckapp.sharedviewmodels.ForgotPasswordSharedViewModel
import com.devsaar.stuckapp.viewmodels.ChangePassword
import com.devsaar.stuckapp.viewmodels.Login
import kotlinx.android.synthetic.main.fragment_login.*


class NewPasswordFragment : BaseFragment<FragmentNewPasswordBinding, ChangePassword>() {
    private lateinit var navController: NavController
    private lateinit var forgotPasswordSharedViewModel: ForgotPasswordSharedViewModel
    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentNewPasswordBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        forgotPasswordSharedViewModel =
            ViewModelProvider(requireActivity()).get(ForgotPasswordSharedViewModel::class.java)
    }

    override fun setListener() {
        binding.apply {
            btnConfirm.setOnClickListener {
                updatePassword()
            }

        }
    }

    override fun getViewModel(): Class<ChangePassword> = ChangePassword::class.java


    fun updatePassword() {

        binding.apply {
            when {
                isEmpty(etNewPassword) -> etNewPassword.error = "Enter New Password"
                isEmpty(etConfirmPassword) -> etConfirmPassword.error = "Confirm Password"
                (etNewPassword.text.toString() != etConfirmPassword.text.toString()) -> {
                    etConfirmPassword.error = "Does not match"
                }
                else -> {

                    loadChangePasswordResponse()
                }

            }
        }
    }

    private fun loadChangePasswordResponse() {
        setVisibility(binding.pb, binding.btnConfirm)
        if (isNetworkConnected()) {


            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[ChangePassword::class.java]
            var mail = ""
            forgotPasswordSharedViewModel.email.observe(viewLifecycleOwner, Observer {
                // updating data in displayMsg
                mail = it
            })
            viewModel.getData(
                repository,
                mail,
                getText(binding.etNewPassword),
            )

            viewModel.changePasswordData.observe(viewLifecycleOwner, {
                setVisibility(binding.btnConfirm, binding.pb)
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            showSnackBar(message)
                        }
                        ResponseStatus.Success.value -> {
                            showSnackBar(message)
                            navigateAndClearBackStack(AuthActivity::class.java)
                        }
                    }
                }


            })

        } else {
            setVisibility(binding.btnConfirm, binding.pb)
        }

    }


}