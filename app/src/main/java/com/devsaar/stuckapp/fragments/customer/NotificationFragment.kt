package com.devsaar.stuckapp.fragments.customer

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.devsaar.stuckapp.adapter.GetNotificationAdapter
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentNotificationBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.enumclasses.SharedPrefConst
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListData
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedPref.SharedPref
import com.devsaar.stuckapp.viewmodels.GetNotification
import kotlinx.android.synthetic.main.fragment_notification.*


class NotificationFragment : BaseFragment<FragmentNotificationBinding, GetNotification>() {
    private var notificationList = mutableListOf<GetBusinessListData>()
    private lateinit var navController: NavController
    private val getNotificationAdapter by lazy {
        GetNotificationAdapter(requireContext(), navController)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentNotificationBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        binding.rvNotification.adapter = getNotificationAdapter
        getNotificationAdapter.addItems(notificationList)
        loadGetBusinessListResponse()
    }

    override fun setListener() {
        binding.apply {
            btn_back.setOnClickListener {
                navController.navigateUp()
            }

        }
    }

    private fun loadGetBusinessListResponse() {
        setVisibility(binding.pb, binding.rvNotification)
        if (isNetworkConnected()) {
            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[GetNotification::class.java]
            viewModel.getData(
                repository,
                SharedPref.instance?.getLoginData(SharedPrefConst.UserDataKey.value)?.id!!
            )

            viewModel.getNotificationData.observe(viewLifecycleOwner, {
                setVisibility(binding.rvNotification, binding.pb)
                notificationList.clear()
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            binding.tvError.visibility = View.VISIBLE
                            binding.tvError.text = message
                        }
                        ResponseStatus.Success.value -> {

                            if (data.isNullOrEmpty()) {
                                binding.tvError.visibility = View.VISIBLE
                                binding.tvError.text = message
                            }
                            for (idea in data) {
                                notificationList.add(idea)
                                Log.d("notification", idea.name)
                            }
                            getNotificationAdapter.notifyDataSetChanged()
                        }
                    }
                }

            })

        } else {
            setVisibility(binding.pb, binding.rvNotification)
        }
    }

    override fun getViewModel(): Class<GetNotification> = GetNotification::class.java


}