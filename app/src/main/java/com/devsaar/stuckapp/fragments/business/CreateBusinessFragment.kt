package com.devsaar.stuckapp.fragments.business

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.adapter.UploadedImagesAdapter
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentCreateBusinessBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.enumclasses.SharedPrefConst
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.models.CreateBusinessJsonRequest
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedPref.SharedPref
import com.devsaar.stuckapp.utils.Utils
import com.devsaar.stuckapp.viewmodels.CreateBusiness
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream


class CreateBusinessFragment : BaseFragment<FragmentCreateBusinessBinding, CreateBusiness>() {
    private lateinit var navController: NavController
    private val imageList = mutableListOf<Bitmap>()
    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_IMAGE_UPLOAD = 2
    var base64List = mutableListOf<String?>()
    private val uploadedImagesAdapter by lazy {
        UploadedImagesAdapter()
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentCreateBusinessBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }

    override fun init() {
        navController = findNavController()
        binding.rvUploadedImages.adapter = uploadedImagesAdapter
        uploadedImagesAdapter.addItems(imageList)

    }

    override fun getViewModel(): Class<CreateBusiness> = CreateBusiness::class.java
    override fun setListener() {
        binding.apply {

            btnCreate.setOnClickListener {
                loadCreateBusinessResponse()
            }
            ivUploadImage.setOnClickListener {
                showMediaSelectionDialog()
            }

        }
    }

    private fun loadCreateBusinessResponse() {
        setVisibility(binding.pb, binding.btnCreate)
        base64List.clear()
        for (i in uploadedImagesAdapter.items) {
            base64List.add(Utils.encodeImage(i))
        }
        if (isNetworkConnected()) {
            val businessData = CreateBusinessJsonRequest(
                getText(binding.etBusinessName),
                getText(binding.etDescription),
                getText(binding.etWebLink),
                base64List,
                SharedPref.instance?.getLoginData(SharedPrefConst.UserDataKey.value)?.id,
                null
            )


            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[CreateBusiness::class.java]
            viewModel.getData(
                repository,
                businessData
            )


            viewModel.createBusinessData.observe(viewLifecycleOwner, {
                setVisibility(binding.btnCreate, binding.pb)
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            showSnackBar(message)
                        }
                        else -> {
                            showSnackBar(message)
                        }
                    }
                }


            })

        } else {
            binding.pb.visibility = View.GONE
        }

    }

    private fun showMediaSelectionDialog() {
        MaterialAlertDialogBuilder(requireContext())
            .setMessage(resources.getString(R.string.supporting_text))
            .setNegativeButton(resources.getString(R.string.Gallery)) { dialog, which ->
                openGallery()
            }
            .setPositiveButton(resources.getString(R.string.camera)) { dialog, which ->
                takePicture()
            }
            .show()
    }

    private fun openGallery() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        val mimeTypes = arrayOf("image/jpeg", "image/png")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        startActivityForResult(intent, REQUEST_IMAGE_UPLOAD)
    }


    private fun takePicture() {
        val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        try {
            this.startActivityForResult(pictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: ActivityNotFoundException) {
            showToast(e.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        try {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == REQUEST_IMAGE_UPLOAD) {
                    val imageUri: Uri = data?.data!!

                    var imageStream: InputStream? = null
                    try {
                        imageStream = context?.contentResolver?.openInputStream(imageUri)
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    }

                    val selectedImageBitmap = BitmapFactory.decodeStream(imageStream)
                    imageList.add(selectedImageBitmap)
                    uploadedImagesAdapter.notifyDataSetChanged()
                }
                if (requestCode == REQUEST_IMAGE_CAPTURE) {

                    if (data?.extras?.get("data") != null) {
                        imageList.add(data.extras?.get("data") as Bitmap)
                        uploadedImagesAdapter.notifyDataSetChanged()

                    } else {
                        showToast("no image selectd")
                    }
                }
            }

        } catch (e: IOException) {
            e.printStackTrace()
        }

    }
}