package com.devsaar.stuckapp.fragments.business

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.adapter.BusinessListAdapter
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentHomeBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListData
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedPref.SharedPref
import com.devsaar.stuckapp.viewmodels.GetBusinessList


class BusinessListFragment : BaseFragment<FragmentHomeBinding, GetBusinessList>() {
    private var businessList = mutableListOf<GetBusinessListData>()
    private lateinit var navController: NavController
    private val businessListAdapter by lazy {
        BusinessListAdapter(requireContext(), navController)
    }
    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentHomeBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        businessList.clear()
        binding.rvBusinessList.adapter = businessListAdapter
        businessListAdapter.addItems(businessList)
        loadGetBusinessListResponse()
    }

    override fun setListener() {
        binding.apply {

            businessListAdapter.listener = { view, item, position ->
                when (view.id) {
                    R.id.l_layout -> {
                        val action =
                            BusinessListFragmentDirections.actionBusinessListFragmentToDetailFragment(
                                businessList[position]
                            )
                        navController.navigate(action)
                    }
                }
            }
        }
    }
    private fun loadGetBusinessListResponse() {
        setVisibility(binding.pb, binding.rvBusinessList)
        if (isNetworkConnected()) {

            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[GetBusinessList::class.java]
            viewModel.getData(
                repository,
                SharedPref.instance?.getLoginData("stucklyAppUser")?.id!!
            )

            viewModel.getBusinessListData.observe(viewLifecycleOwner, {
                setVisibility(binding.rvBusinessList, binding.pb)
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            binding.tvError.visibility = View.VISIBLE
                            binding.tvError.setText(message)
                        }
                        ResponseStatus.Success.value  -> {
                            for (idea in data) {
                                businessList.add(idea)
                            }
                            businessListAdapter.notifyDataSetChanged()
                        }
                    }
                }

            })

        }else {
            binding.pb.visibility = View.GONE
        }
    }

    override fun getViewModel(): Class<GetBusinessList> = GetBusinessList::class.java


}