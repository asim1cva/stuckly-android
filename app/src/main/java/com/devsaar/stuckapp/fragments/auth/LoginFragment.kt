package com.devsaar.stuckapp.fragments.auth

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.devsaar.stuckapp.activities.business.BusinessDashboardActivity
import com.devsaar.stuckapp.activities.customer.CustomerDashboardActivity
import com.devsaar.stuckapp.activities.forgotpassword.ForgotPasswordActivity
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentLoginBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.enumclasses.SharedPrefConst
import com.devsaar.stuckapp.enumclasses.UserType
import com.devsaar.stuckapp.fcmservice.FCMHandler
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedPref.SharedPref
import com.devsaar.stuckapp.sharedPref.SharedPrefHelper
import com.devsaar.stuckapp.viewmodels.Login
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.fragment_login.*


class LoginFragment : BaseFragment<FragmentLoginBinding, Login>() {
    private lateinit var auth: FirebaseAuth
    val args: LoginFragmentArgs by navArgs()
    private lateinit var navController: NavController

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentLoginBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getFCMToken()
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        auth = Firebase.auth
    }

    override fun setListener() {
        binding.apply {
            tvSignUp.setOnClickListener {
                val action =
                    LoginFragmentDirections.actionLoginFragmentToSignUpFragment(args.userRole)
                navController.navigate(action)
            }
            btnSignin.setOnClickListener {
                login()
            }
            tvForgetPassword.setOnClickListener {
                navigateAndClearBackStack(ForgotPasswordActivity::class.java)
            }

        }
    }

    override fun getViewModel(): Class<Login> = Login::class.java

    // Get FCM token
    fun getFCMToken() {

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            //On token fetch fail
            if (!task.isSuccessful) {
                //msg_token_failed
                val token = task.exception.toString()
                SharedPrefHelper.instance!!.saveDeviceToken(token)

                return@OnCompleteListener
            }

            // Get new Instance ID token
            val newDeviceToken = task.result
            SharedPrefHelper.instance!!.saveDeviceToken(newDeviceToken)
            Log.d("SigninFragment", " new Device token $newDeviceToken")
        })
    }

    fun login() {
        val fcm = FCMHandler()
        fcm.enableFCM()
        binding.apply {
            when {
                isEmpty(etMail) -> etMail.error = "Enter User Mail"
                isEmpty(etPassword) -> etPassword.error = "Enter User Password"
                else -> {
                    loadLoginResponse()
                }

            }
        }
    }

    private fun loadLoginResponse() {
        setVisibility(binding.pb, binding.btnSignin)
        if (isNetworkConnected()) {


            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[Login::class.java]
            viewModel.getData(
                repository,
                getText(binding.etMail),
                getText(binding.etPassword),
                args.userRole,
                SharedPrefHelper.instance!!.deviceToken
            )
            Log.d("userrole", args.userRole)

            viewModel.loginData.observe(viewLifecycleOwner, {
                setVisibility(binding.btnSignin, binding.pb)
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            showSnackBar(message)
                        }
                        ResponseStatus.Success.value -> {
                            SharedPref.instance?.saveLoginData(SharedPrefConst.UserDataKey.value, data)
                            navigateToHomeFragment()
                        }
                    }
                }


            })

        } else {
            setVisibility(binding.btnSignin, binding.pb)
        }

    }

    private fun navigateToHomeFragment() {

        when (args.userRole) {
            UserType.Business.value -> {
                navigateAndClearBackStack(
                    BusinessDashboardActivity::class.java
                )
            }
            UserType.Customer.value -> {
                navigateAndClearBackStack(
                    CustomerDashboardActivity::class.java
                )
            }
            else -> showSnackBar("not recognized")
        }
    }


}