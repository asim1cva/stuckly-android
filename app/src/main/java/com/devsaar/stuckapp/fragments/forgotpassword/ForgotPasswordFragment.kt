package com.devsaar.stuckapp.fragments.forgotpassword

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentForgotPasswordBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedviewmodels.ForgotPasswordSharedViewModel
import com.devsaar.stuckapp.viewmodels.ForgotPassword


class ForgotPasswordFragment : BaseFragment<FragmentForgotPasswordBinding, ForgotPassword>() {
    private lateinit var navController: NavController
    private lateinit var forgotPasswordSharedViewModel: ForgotPasswordSharedViewModel

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentForgotPasswordBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        forgotPasswordSharedViewModel =
            ViewModelProvider(requireActivity()).get(ForgotPasswordSharedViewModel::class.java)
    }

    override fun setListener() {
        binding.apply {
            btnResetPassword.setOnClickListener {
                resetPassword()
            }

        }
    }

    override fun getViewModel(): Class<ForgotPassword> = ForgotPassword::class.java


    fun resetPassword() {
        binding.apply {
            when {
                isEmpty(etMail) -> etMail.error = "Enter User Mail"
                else -> {
                    loadForgotPasswordResponse()

                }

            }
        }
    }

    private fun loadForgotPasswordResponse() {
        setVisibility(binding.pb, binding.btnResetPassword)
        if (isNetworkConnected()) {


            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[ForgotPassword::class.java]
            viewModel.getData(
                repository,
                getText(binding.etMail)
            )

            viewModel.forgotPasswordData.observe(viewLifecycleOwner, {
                setVisibility(binding.btnResetPassword, binding.pb)
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            showSnackBar(message)
                        }
                        ResponseStatus.Success.value -> {
                            forgotPasswordSharedViewModel.setData(
                                getText(binding.etMail),
                                data.code
                            )
                            val action =
                                ForgotPasswordFragmentDirections.actionForgotPasswordFragmentToVerifyEmailFragment()
                            navController.navigate(action)
                        }
                    }
                }


            })

        } else {
            setVisibility(binding.btnResetPassword, binding.pb)
        }

    }


}