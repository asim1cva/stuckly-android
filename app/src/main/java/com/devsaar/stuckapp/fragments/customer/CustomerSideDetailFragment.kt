package com.devsaar.stuckapp.fragments.customer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.adapter.SliderAdapter
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentDetailBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.enumclasses.SharedPrefConst
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedPref.SharedPref
import com.devsaar.stuckapp.viewmodels.FollowBusiness
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations


class CustomerSideDetailFragment : BaseFragment<FragmentDetailBinding, FollowBusiness>() {

    private var isFollow = false
    private val args: CustomerSideDetailFragmentArgs by navArgs()
    private lateinit var navController: NavController
    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentDetailBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        binding.lifecycleOwner=viewLifecycleOwner
        binding.apply {
            if (args.business.isfollowed) {
                isFollow = true
                btnFollow.setImageResource(R.drawable.ic_baseline_favorite_24)
                tvFollow.text = getString(R.string.follow)
            }
            btnSetup.visibility = View.GONE
            btnEdit.visibility = View.GONE
            business = args.business
            val sliderAdapter = SliderAdapter(args.business.images,navController)
            imageSlider.setSliderAdapter(sliderAdapter)
            imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM)
            imageSlider.setSliderTransformAnimation(SliderAnimations.DEPTHTRANSFORMATION)
            imageSlider.startAutoCycle()
        }
    }

    override fun setListener() {
        binding.apply {
            btnBack.setOnClickListener {
                when(args.fragmentname) {
                    getString(R.string.notification) -> {
                        navController.navigateUp()
                    }
                    getString(R.string.customerList) -> {
                        val action =
                            CustomerSideDetailFragmentDirections.actionCustomerSideDetailFragmentToCustomerListingFragment()
                        navController.navigate(action)
                    }
                }

            }

            btnFollow.setOnClickListener {

                followBusinessResponse()
            }

        }
    }

    private fun followBusinessResponse() {
        if (isNetworkConnected()) {


            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[FollowBusiness::class.java]
            viewModel.getData(
                repository,
                SharedPref.instance?.getLoginData(SharedPrefConst.UserDataKey.value)?.id!!,
                args.business.id
            )

            viewModel.followBusinessData.observe(viewLifecycleOwner, {
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            showToast(message)
                        }
                        ResponseStatus.Success.value -> {

                        }
                    }
                }


            })
            if (isFollow) {
                binding.btnFollow.setImageResource(R.drawable.ic_baseline_favorite_border_24)
                binding.tvFollow.text = getString(R.string.following)
                isFollow=false
            } else {
                binding.btnFollow.setImageResource(R.drawable.ic_baseline_favorite_24)
                binding.tvFollow.text = getString(R.string.follow)
                isFollow=true
            }

        }
    }

    override fun getViewModel(): Class<FollowBusiness> = FollowBusiness::class.java
}