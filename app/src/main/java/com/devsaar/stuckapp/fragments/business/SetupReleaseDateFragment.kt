package com.devsaar.stuckapp.fragments.business

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentSetupReleaseDateBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.utils.Utils
import com.devsaar.stuckapp.viewmodels.SetUpReleaseDate


class SetupReleaseDateFragment : BaseFragment<FragmentSetupReleaseDateBinding, SetUpReleaseDate>() {
    val args: SetupReleaseDateFragmentArgs by navArgs()
    private lateinit var navController: NavController

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentSetupReleaseDateBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        binding.etDate.setText(Utils.currentDate)
        when{
            args.business.release_date!=null -> {
                binding.etDate.setText(args.business.release_date)
                binding.etMessage.setText(args.business.release_message)
                binding.etDetail.setText(args.business.release_detail)
            }

        }
    }

    override fun setListener() {
        binding.apply {
            btnBack.setOnClickListener {
                val action =
                    SetupReleaseDateFragmentDirections.actionSetupReleaseDateFragmentToDetailFragment(args.business)
                navController.navigate(action)
            }
            etDate.setOnClickListener {
                Utils.selectDate(requireContext(), etDate)
            }
            btnConfirm.setOnClickListener {
                loadSetReleaseDateResponse()
            }


        }
    }


    private fun loadSetReleaseDateResponse() {
        setVisibility(binding.pb, binding.btnConfirm)
        if (isNetworkConnected()) {


            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[SetUpReleaseDate::class.java]
            viewModel.getData(
                repository,
                args.business.id,
                getText(binding.etDate),
                getText(binding.etMessage),
                getText(binding.etDetail)
            )

            viewModel.setReleaseDateData.observe(viewLifecycleOwner, {
                setVisibility(binding.btnConfirm, binding.pb)
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            showToast(message)
                        }
                        else -> {
                            showToast(message)
                            val action=SetupReleaseDateFragmentDirections.actionSetupReleaseDateFragmentToBusinessListFragment()
                            navController.navigate(action)
                        }
                    }
                }


            })

        } else {
            binding.pb.visibility = View.GONE
        }

    }

    override fun getViewModel(): Class<SetUpReleaseDate> = SetUpReleaseDate::class.java
}