package com.devsaar.stuckapp.fragments.business

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.adapter.SliderAdapter
import com.devsaar.stuckapp.base.BaseFragmentWithoutVM
import com.devsaar.stuckapp.databinding.FragmentDetailBinding
import com.devsaar.stuckapp.utils.Utils
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations


class DetailFragment : BaseFragmentWithoutVM<FragmentDetailBinding>() {


    val args: DetailFragmentArgs by navArgs()
    private lateinit var navController: NavController
    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentDetailBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        binding.apply {
            if (!args.business.release_date.isNullOrEmpty()) {
                lRelease.visibility = View.VISIBLE
                args.business.apply {
                    tvReleaseDate.text = release_date
                    tvReleaseMessage.text = release_message
                    tvReleaseDescription.text=release_detail
                }
                btnSetup.text = getString(R.string.change_release_date)
            }
            btnFollow.visibility = View.GONE
            tvFollow.visibility = View.GONE
            business = args.business
            val sliderAdapter = SliderAdapter(args.business.images,navController)
            imageSlider.setSliderAdapter(sliderAdapter)
            imageSlider.setIndicatorAnimation(IndicatorAnimationType.WORM)
            imageSlider.setSliderTransformAnimation(SliderAnimations.DEPTHTRANSFORMATION)
            imageSlider.startAutoCycle()
        }
    }

    override fun setListener() {
        binding.apply {
            btnBack.setOnClickListener {
                val action =
                    DetailFragmentDirections.actionDetailFragmentToBusinessListFragment()
                navController.navigate(action)
            }

            btnEdit.setOnClickListener {
                val action =
                    DetailFragmentDirections.actionDetailFragmentToUpdateBusinessFragment(args.business)
                navController.navigate(action)

            }

            btnSetup.setOnClickListener {
                val action =
                    DetailFragmentDirections.actionDetailFragmentToSetupReleaseDateFragment(args.business)
                navController.navigate(action)
            }

        }
    }


}