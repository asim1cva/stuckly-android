package com.devsaar.stuckapp.fragments.customer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.adapter.BusinessListAdapter
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentCustomerListingBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.enumclasses.SharedPrefConst
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.models.getbusinesslist.GetBusinessListData
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedPref.SharedPref
import com.devsaar.stuckapp.viewmodels.GetBusinessList


class CustomerListingFragment : BaseFragment<FragmentCustomerListingBinding, GetBusinessList>() {

    private var customerBusinessList = mutableListOf<GetBusinessListData>()
    private lateinit var navController: NavController
    private val businessListAdapter by lazy {
        BusinessListAdapter(requireContext(), navController)
    }

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentCustomerListingBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        customerBusinessList.clear()
        binding.rvBusinessList.adapter = businessListAdapter
        businessListAdapter.addItems(customerBusinessList)
        loadUserBusinessListResponse()
    }

    override fun setListener() {
        binding.apply {


            ivNotification.setOnClickListener {
                val action =
                    CustomerListingFragmentDirections.actionCustomerListingFragmentToNotificationFragment()
                navController.navigate(action)
            }
            businessListAdapter.listener = { view, item, position ->
                when (view.id) {
                    R.id.l_layout -> {
                        val action =
                            CustomerListingFragmentDirections.actionCustomerListingFragmentToCustomerSideDetailFragment(
                                customerBusinessList[position],getString(R.string.customerList)
                            )
                        navController.navigate(action)
                    }
                }
            }

        }
    }

    override fun getViewModel(): Class<GetBusinessList> = GetBusinessList::class.java
    private fun loadUserBusinessListResponse() {
        setVisibility(binding.pb, binding.rvBusinessList)
        if (isNetworkConnected()) {

            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[GetBusinessList::class.java]
            viewModel.getData(
                repository,
                SharedPref.instance?.getLoginData(SharedPrefConst.UserDataKey.value)?.id!!
            )

            viewModel.getBusinessListData.observe(viewLifecycleOwner, {
                setVisibility(binding.rvBusinessList, binding.pb)
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            binding.tvError.visibility = View.VISIBLE
                            binding.tvError.text = message
                        }
                        ResponseStatus.Success.value -> {
                            for (idea in data) {
                                customerBusinessList.add(idea)
                            }
                            businessListAdapter.notifyDataSetChanged()
                        }
                    }
                }

            })

        } else {
            binding.pb.visibility = View.GONE
        }
    }

}