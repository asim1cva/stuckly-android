package com.devsaar.stuckapp.fragments.business

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.devsaar.stuckapp.R
import com.devsaar.stuckapp.activities.auth.AuthActivity
import com.devsaar.stuckapp.base.BaseFragment
import com.devsaar.stuckapp.databinding.FragmentProfileBinding
import com.devsaar.stuckapp.enumclasses.ResponseStatus
import com.devsaar.stuckapp.enumclasses.SharedPrefConst
import com.devsaar.stuckapp.fcmservice.FCMHandler
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.network.RetrofitClient
import com.devsaar.stuckapp.repository.Repository
import com.devsaar.stuckapp.sharedPref.SharedPref
import com.devsaar.stuckapp.utils.Utils
import com.devsaar.stuckapp.viewmodels.EditProfile
import com.google.android.material.dialog.MaterialAlertDialogBuilder


class ProfileFragment : BaseFragment<FragmentProfileBinding, EditProfile>() {

    private lateinit var navController: NavController

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentProfileBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
        binding.user = SharedPref.instance?.getLoginData(SharedPrefConst.UserDataKey.value)!!
    }

    override fun setListener() {
        binding.apply {
            etDate.setOnClickListener {
                Utils.selectDateEt(requireContext(), etDate)
            }
            btnUpdateChanges.setOnClickListener {
                updateProfile()
            }
            ivLogout.setOnClickListener {
                logOut()
            }

        }
    }

    override fun getViewModel(): Class<EditProfile> = EditProfile::class.java
   private fun updateProfile() {
        binding.apply {
            when {
                (isEmpty(etMobile)) -> {
                    etMobile.setText("")
                }

                (isEmpty(etFullname)) -> {
                    etFullname.setText("")
                }

                (isEmpty(etEmail)) -> {
                    etEmail.setText("")
                }
                (isEmpty(etDate)) -> {
                    etEmail.setText("")
                }
                else -> loadEditProfileResponse()
            }
        }
    }
    private fun loadEditProfileResponse() {
        setVisibility(binding.pb, binding.btnUpdateChanges)
        if (isNetworkConnected()) {


            val service = RetrofitClient.getInstance()?.create(APIService::class.java)

            val repository = Repository(service!!)

            viewModel = ViewModelProvider(
                this
            )[EditProfile::class.java]
            viewModel.getData(
                repository,
                SharedPref.instance?.getLoginData(SharedPrefConst.UserDataKey.value)?.id!!,
                getText(binding.etFullname),
                getText(binding.etEmail),
                getText(binding.etDate),
                getText(binding.etMobile),
            )

            viewModel.editProfileData.observe(viewLifecycleOwner, {
                setVisibility(binding.btnUpdateChanges, binding.pb)
                it?.apply {
                    when (status) {
                        ResponseStatus.Error.value -> {
                            showToast(message)
                        }
                        else -> {
                            showToast(message)
                            SharedPref.instance?.saveLoginData(SharedPrefConst.UserDataKey.value, data)
                        }
                    }
                }


            })

        } else {
            binding.pb.visibility = View.GONE
        }

    }
    private fun logOut() {

        MaterialAlertDialogBuilder(requireContext())
            .setMessage(resources.getString(R.string.logout_text))
            .setNegativeButton(resources.getString(R.string.Cancel)) { dialog, which ->

            }
            .setPositiveButton(resources.getString(R.string.Accept)) { dialog, which ->
                val fcm= FCMHandler()
                fcm.disableFCM()
                SharedPref.instance!!.clearSharedPref()
                navigateAndClearBackStack(AuthActivity::class.java)
            }
            .show()
    }

        }