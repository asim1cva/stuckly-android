package com.devsaar.stuckapp.fragments.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.devsaar.stuckapp.base.BaseFragmentWithoutVM
import com.devsaar.stuckapp.databinding.FragmentWelcomeScreenBinding


class WelcomeScreenFragment : BaseFragmentWithoutVM<FragmentWelcomeScreenBinding>() {

    private lateinit var navController: NavController

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentWelcomeScreenBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
    }

    override fun setListener() {
        binding.apply {
            btnContinue.setOnClickListener {
                val action =
                    WelcomeScreenFragmentDirections.actionWelcomeScreenFragmentToChooseFragment()
                navController.navigate(action)
            }

        }
    }


}