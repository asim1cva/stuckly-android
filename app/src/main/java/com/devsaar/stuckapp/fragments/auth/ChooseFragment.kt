package com.devsaar.stuckapp.fragments.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.devsaar.stuckapp.base.BaseFragmentWithoutVM
import com.devsaar.stuckapp.databinding.FragmentChooseBinding


class ChooseFragment : BaseFragmentWithoutVM<FragmentChooseBinding>() {

    private lateinit var navController: NavController

    override fun getViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ) = FragmentChooseBinding.inflate(inflater, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        setListener()

    }


    override fun init() {
        navController = findNavController()
    }

    override fun setListener() {
        binding.apply {

            btnBusiness.setOnClickListener {
                val action =
                    ChooseFragmentDirections.actionChooseFragmentToLoginFragment("business")
                navController.navigate(action)
            }
            btnCustomer.setOnClickListener {
                val action =
                    ChooseFragmentDirections.actionChooseFragmentToLoginFragment("customer")
                navController.navigate(action)
            }

        }
    }


}