package com.devsaar.stuckapp.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.devsaar.stuckapp.interfaces.APIService
import com.devsaar.stuckapp.models.CreateBusinessJsonRequest
import retrofit2.http.Body
import retrofit2.http.Field

class Repository(private val apiService: APIService) {
    private val responseLiveData = MutableLiveData<Any>()
    val response: LiveData<Any> get() = responseLiveData

    suspend fun signUp(
        role: String, userName: String, email: String, password: String,
        phoneNumber: String, fcmToken: String
    ) = apiService.signUp(
        role, userName, email, password, phoneNumber, fcmToken
    )

    suspend fun login(email: String, password: String, role: String, fcm_token: String) =
        apiService.userLogin(email, password, role, fcm_token)

    suspend fun editProfile(
        userId: Int, userName: String, email: String, dateOfBirth: String,
        phoneNumber: String
    ) = apiService.editProfile(
        userId, userName, email, dateOfBirth, phoneNumber
    )

    suspend fun userBusinessList(
        userId: Int
    ) = apiService.userBusinessList(userId)

    suspend fun getBusinessList(
        userId: Int
    ) = apiService.getBusinessList(userId)


    suspend fun setReleaseDate(
        businessId: Int,
        date: String,
        message: String,
        detail: String,
    ) = apiService.setReleaseDate(businessId, date, message, detail)

    suspend fun followBusiness(
        businessId: Int,
        userId: Int
    ) = apiService.followBusiness(businessId, userId)

    suspend fun createBusiness(
        businessData: CreateBusinessJsonRequest
    ) = apiService.createBusiness(businessData)

    suspend fun updateBusiness(
        businessData: CreateBusinessJsonRequest
    ) = apiService.updateBusiness(businessData)

    suspend fun forgotPassword(
        email: String,
    ) = apiService.forgotPassword(email)

    suspend fun changePassword(
        email: String,
        newPassword: String,
    ) = apiService.changePassword(email, newPassword)

    suspend fun getNotification(
        userId: Int
    ) = apiService.getNotification(userId)
}