package com.devsaar.stuckapp.sharedPref


class SharedPrefHelper {
    //login preferences
    fun saveDeviceToken(value: String?) {
        SharedPref.instance!!.saveValue(DEVICE_TOKEN, value)
    }

    val deviceToken: String
        get() = SharedPref.instance!!.getStringValue(DEVICE_TOKEN)!!

    fun saveIntegerValue(value: Int) {
        SharedPref.instance!!.saveValue(ABC_VALUE, value)
    }

    val integerValue: Int
        get() = SharedPref.instance!!.getIntValue(ABC_VALUE)


    fun saveLoginState(isTrue: Boolean) {
        SharedPref.instance!!.saveValue(IS_LOGIN, isTrue)
    }

    //login preferences
    val isLoggedIn: Boolean
        get() = SharedPref.instance!!.getBoolValue(IS_LOGIN)


    fun savePassword(password: String) {
        SharedPref.instance!!.saveValue(PASSWORD, password)
    }

    val getPassword: String?
        get() = SharedPref.instance!!.getStringValue(PASSWORD)

    /* fun saveIntroductionState(isTrue: Boolean) {
         SharedPref.instance!!.saveValue(IS_INTRO, isTrue)
     }

     val isIntroduction: Boolean
         get() = SharedPref.instance!!.getBoolValue(IS_INTRO)*/

    fun saveCouponApply(isTrue: Boolean) {
        SharedPref.instance!!.saveValue(IS_APPLY_COUPON, isTrue)
    }

    //login preferences
    val isCouponApplied: Boolean
        get() = SharedPref.instance!!.getBoolValue(IS_APPLY_COUPON)


//    fun saveLoginUser(user: UserResponse) {
//        SharedPref.instance!!.saveLoginData(LOGIN_DATA, user)
//    }
//
//    val getLoginUserData: UserResponse
//        get() = SharedPref.instance!!.getLoginData(LOGIN_DATA)

    companion object {
        //add key here
        private const val LOGIN_TYPE = "LOGIN_TYPE"
        private const val ABC_VALUE = "ABC_VALUE"
        private const val IS_LOGIN = "IS_LOGIN"
        private const val PASSWORD = "password"
        private const val IS_INTRO = "IS_INTRO"
        private const val IS_APPLY_COUPON = "IS_APP_INTRO"
        private const val LOGIN_DATA = "LOGIN_DATA"
        private const val DEVICE_TOKEN = "DEVICE_TOKEN"
        var instance: SharedPrefHelper? = null
            get() {
                if (field == null) {
                    synchronized(SharedPrefHelper::class.java) {
                        if (field == null) {
                            field = SharedPrefHelper()
                        }
                    }
                }
                return field
            }
            private set
    }
}