package com.devsaar.stuckapp.sharedPref

import android.content.Context
import com.google.gson.Gson
import com.devsaar.stuckapp.activities.MyApp.Companion.context
import com.devsaar.stuckapp.models.auth.AuthData


class SharedPref  //Private Constructor
private constructor() {
    private val sharedPreferences = context.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
    private val editor = sharedPreferences.edit()
    fun saveValue(key: String?, value: String?) {
        editor.putString(key, value)
        editor.apply()
    }

    fun saveValue(key: String?, value: Int) {
        editor.putInt(key, value)
        editor.apply()
    }

    fun saveValue(key: String?, value: Boolean) {
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun getStringValue(key: String?): String? {
        return sharedPreferences.getString(key, "")
    }

    fun getIntValue(key: String?): Int {
        return sharedPreferences.getInt(key, -1)
    }

    fun clearSharedPref() {
        editor.clear().apply()
    }

    fun getBoolValue(key: String?): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

    fun saveLoginData(key: String?, user: AuthData) {
        val gson = Gson()
        val json = gson.toJson(user)
        editor.putString(key, json)
        editor.apply()
    }

    fun getLoginData(key: String?): AuthData? {
        val gson = Gson()
        val json = sharedPreferences.getString(key, null)
        if (json==null){
            return null
        }
        return gson.fromJson(json, AuthData::class.java)

    }


    companion object {
        private const val SHARED_PREF = "GameLeague"
        private var mInstance: SharedPref? = null

        @get:Synchronized
        val instance: SharedPref?
            get() {
                if (mInstance == null) {
                    mInstance = SharedPref()
                }
                return mInstance
            }
    }
}