package com.devsaar.stuckapp.models.auth

data class AuthResponse(
    val `data`: AuthData,
    val message: String,
    val status: Int
)