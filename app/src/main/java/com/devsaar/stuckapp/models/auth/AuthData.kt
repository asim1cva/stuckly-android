package com.devsaar.stuckapp.models.auth

data class AuthData(
    val created_at: String,
    val date_of_birth: Any,
    val email: String,
    val email_verified_at: Any,
    val fcm_token: String,
    val id: Int,
    val phone_number: String,
    val role: String,
    val updated_at: String,
    val user_name: String
)