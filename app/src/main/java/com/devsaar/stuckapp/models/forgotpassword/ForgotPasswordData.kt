package com.devsaar.stuckapp.models.forgotpassword

data class ForgotPasswordData(
    val code: Int
)