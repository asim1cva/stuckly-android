package com.devsaar.stuckapp.models.dateslider

data class Date(val day: String, val dayDate: String,val date: String, var selected: Boolean)
