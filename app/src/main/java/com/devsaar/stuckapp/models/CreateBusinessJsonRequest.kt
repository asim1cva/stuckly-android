package com.devsaar.stuckapp.models

import com.google.gson.annotations.SerializedName

data class CreateBusinessJsonRequest(
    @SerializedName("name") val name: String?,
    @SerializedName("description") val description: String?,
    @SerializedName("website_link") val websiteLink: String?,
    @SerializedName("image") val image: List<String?>,
    @SerializedName("user_id") val userId: Int?,
    @SerializedName("business_id") val businessId: Int?

)