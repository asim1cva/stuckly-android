package com.devsaar.stuckapp.models.getbusinesslist

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetBusinessListImage(
    val business_id: String,
    val image_id: String,
    val image_url: String,
    val name: String
): Parcelable