package com.devsaar.stuckapp.models.getbusinesslist

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetBusinessListData(
    val created_at: String,
    val description: String,
    val followers_count: Int,
    val id: Int,
    val image_url: String,
    val images: List<GetBusinessListImage>,
    val isfollowed: Boolean,
    val name: String,
    val release_date: String,
    val release_detail: String,
    val release_message: String,
    val updated_at: String,
    val user_id: Int,
    val website_link: String
):Parcelable