package com.devsaar.stuckapp.models.getbusinesslist

data class GetBusinessListResponse(
    val `data`: List<GetBusinessListData>,
    val message: String,
    val status: Int
)