package com.devsaar.stuckapp.models.forgotpassword

data class ForgotPasswordResponse(
    val `data`: ForgotPasswordData,
    val message: String,
    val status: Int
)