package com.devsaar.stuckapp.models.getbusinesslist

import com.devsaar.stuckapp.models.forgotpassword.ForgotPasswordData

data class ForgotPasswordResponse(
    val `data`: ForgotPasswordData,
    val message: String,
    val status: Int
)